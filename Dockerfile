FROM node:latest

WORKDIR /usr/src

COPY package*.json ./
COPY ./src ./src

RUN npm install

COPY . .

EXPOSE 5000
CMD [ "npm", "start" ]