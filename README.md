**# neo4j_final**

## Desplegar con Docker

Al clonar el proyecto:

Comprobar que la url del servidor del driver es:

- En el archivo **bookRoutes.js**: `'bolt://neo4j_library'` 
- En los archivos **header.ejs, graphBookAuthor.ejs, graphBooksByAuthor, graphCategory**: `"bolt://localhost:11003"`

Una vez comprobado esto, correr dentro de la carpeta de dicho proyecto el comando:

  `docker-compose up (Asegurarse que la app 'Docker Desktop' se encuentra abierta)`

Una vez levantada la aplicación debemos configurar la base de datos:

1.   Acceder a http://localhost:11004/

2.   Ingresar credenciales:
   `bolt://localhost:11003`

   ``username: neo4j`

   `password: neo4j`

3. Ahora introducir la nueva contraseña: **1234**

4. Acceder con usuario-> **neo4j** y contraseña->**1234** 

5. Una vez dentro, utilizar la consulta -> `:use system`

6. Ahora, para crear los diferentes usuarios con sus diferentes roles y comprobar la funcionalidad de esto en la aplicación, introducir las siguiente consultas **UNA POR UNA**. Sin modificar nada de ellas.

   ​    -<u>*CREACIÓN DEL PRIMER USUARIO*</u>

   ​      `create user user set password 'abc' CHANGE NOT REQUIRED`

   ​      `GRANT ROLE reader TO user;`

   ​      `GRANT ACCESS ON DATABASE * TO reader`

   ​      `GRANT MATCH {*} ON GRAPH  * TO reader`

   ​    -*<u>CREACIÓN DE USUARIO EDITOR</u>*

   ​      `GRANT ACCESS ON DATABASE * TO editor`

   ​      `GRANT ALL GRAPH PRIVILEGES ON GRAPH * TO editor`

7. Cuando se han creado los usuarios debemos crear la base de datos:

   ​    -Utilizar la consulta -> `:use neo4j` 

   ​    -Copiar y pegar lo que esta dentro del archivo **library-data.cypher** que esta dentro de la carpeta **src/cypher/library-data.cypher** dentro del query builder y correrlo.

   ​	 (<u>Al ser una imagen dockerizada de Neo4j, no permite agregar archivos a la base de datos. Más adelante se verá como implementarlo con *Neo4J Desktop*</u>)

8. Ya que la base de datos esta configurada:

   ​	-Acceder al **localhost:5000**, en donde esta levantada la aplicación.

   ​	-Para el login:

   - Acceder como **administrador**:

     Username: neo4j

     Password: 1234

   - Acceder como **editor**:

     Username: editor

     Password: abc

   - Acceder como **lector** (al solo tener permiso para leer de la base de datos, se elimina la opción de agregar y eliminar que hay al acceder con los otros usuarios):

     Username: user

     Password: abc

   ## Desplegar en local

   Al clonar el proyecto:

   Comprobar que la url del servidor del driver es:

   - En el archivo **bookRoutes.js**: `'bolt://localhost:7687'` 
   - En los archivos **header.ejs, graphBookAuthor.ejs, graphBooksByAuthor, graphCategory**: `"bolt://localhost:7687"`

   Una vez comprobado esto, correr dentro de la carpeta de dicho proyecto el comando:

   `npm install`

   `npm run start`

   Una vez levantada la aplicación debemos configurar la base de datos:

   1. Iniciar la aplicación "Neo4j Desktop"

   2.   Ingresar credenciales:

      - `bolt://localhost:7687`
      - `username: neo4j`
      - `password: neo4j`

   3. Ahora introducir la nueva contraseña: **1234**

   4. Acceder con usuario-> **neo4j** y contraseña->**1234** 

   5. Una vez dentro, utilizar la consulta -> `:use system`

   6. Ahora, para crear los diferentes usuarios con sus diferentes roles y comprobar la funcionalidad de esto en la aplicación, introducir las siguiente consultas **UNA POR UNA**. Sin modificar nada de ellas.

      ​    -<u>*CREACIÓN DEL PRIMER USUARIO*</u>

      ​      `create user user set password 'abc' CHANGE NOT REQUIRED`

      ​      `GRANT ROLE reader TO user;`

      ​      `GRANT ACCESS ON DATABASE * TO reader`

      ​      `GRANT MATCH {*} ON GRAPH  * TO reader`

      ​    -*<u>CREACIÓN DE USUARIO EDITOR</u>*

      ​      `GRANT ACCESS ON DATABASE * TO editor`

      ​      `GRANT ALL GRAPH PRIVILEGES ON GRAPH * TO editor`

   7. Cuando se han creado los usuarios debemos crear la base de datos:

      ​    -Utilizar la consulta -> `:use neo4j` 

   8. Importar el archivo **library-data.cypher** que esta dentro de la carpeta **src/cypher/library-data.cypher** dentro de la base de datos. Una vez importado, hacer click en *run*.

   9. Ya que la base de datos esta configurada:

      ​	-Acceder al **localhost:5000**, en donde esta levantada la aplicación.

      ​	-Para el login:

      - Acceder como **administrador**:

        Username: neo4j

        Password: 1234

      - Acceder como **editor**:

        Username: editor

        Password: abc

      - Acceder como **lector** (al solo tener permiso para leer de la base de datos, se elimina la opción de agregar y eliminar que hay al acceder con los otros usuarios):

        Username: user

        Password: abc

   ##### La funcionalidad de cada usuario se explicará mejor en la memoria del proyecto.

   Advertencias:

   1. Si al hacer click en uno de los libros se muestran algunos espacios de la información de dicho libro en blanco, actualizar la página. 
   2. Si al agregar un libro, este no se muestra en la página principal, actualizar la página.
   3. Si al eliminar un libro este no desaparece de la página principal, actualizar la página.

   