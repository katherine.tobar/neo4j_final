// load books
// This is an initialization script for the book graph.
// Run it only once.
// Have you run it twice? Use `MATCH (n) WHERE (n:Person OR n:Movie) DETACH DELETE n` to start over.
CREATE (Stephen:Author {name:"Stephen King", born:1947})
CREATE (IT:Book {title:'IT', published:1986, pages:1502, img:'https://m.media-amazon.com/images/I/41nngxCNKxL.jpg'})
CREATE (TheShining:Book {title:'The shining', published:1977, pages:447, img:'https://i.pinimg.com/564x/55/23/e6/5523e6b39d0a1d1118877eaf23a8c23b.jpg'})
CREATE (Cell:Book {title:'Cell', published:2006, pages:350, img:'https://imagessl9.casadellibro.com/a/l/t5/19/9788483465219.jpg'})
CREATE (Misery:Book {title:'Misery', published:1987, pages:420, img:'https://imagessl3.casadellibro.com/a/l/t7/53/9788497595353.jpg'})
CREATE (MrMercedes:Book {title:'Mr. Mercedes', published:2014, pages:436, img:'https://images-na.ssl-images-amazon.com/images/I/51PA+G5UWmL._SX301_BO1,204,203,200_.jpg'})
CREATE (LiseysStory:Book {title:'Liseys Story', published:2006, pages:528, img:'https://images-na.ssl-images-amazon.com/images/I/41Lw3AHMAEL._SX328_BO1,204,203,200_.jpg'})
CREATE (John:Author {name:"John Katzenbach", born:1950})
CREATE (TheAnalyst:Book {title:'The Analyst', published:2002, pages:512, img:'https://images-na.ssl-images-amazon.com/images/I/41oP2xkIuwL._SX333_BO1,204,203,200_.jpg'})
CREATE (TheDeadStudent:Book {title:'The Dead Student', published:2015, pages:485, img:'https://images-na.ssl-images-amazon.com/images/I/51iNJRXzEkL.jpg'})
CREATE (William:Author {name:"William Shakespeare", born:1564})
CREATE (RomeoAndJuliet:Book {title:'Romeo and Juliet', published:1595, pages:480, img:'https://www.memoriapress.com/wp-content/uploads/2017/01/Romeo-Juliet_student-6x9-1.jpg'})
CREATE (KingJohn:Book {title:'King John', published:1597, pages:400, img:'https://m.media-amazon.com/images/I/51lzY0yE8BL.jpg'})
CREATE (JuliusCaesar:Book {title:'Julius Caesar', published:1600, pages:239, img:'https://m.media-amazon.com/images/I/51cgd89I9qL.jpg'})
CREATE (Edgar:Author {name:"Edgar Allan Poe", born:1809})
CREATE (Berenice:Book {title:'Berenice', published:1835, pages:340, img:'https://images-na.ssl-images-amazon.com/images/I/91f+6zZB-aL.jpg'})
CREATE (Harper:Author {name:"Harper Lee", born:1926})
CREATE (ToKillAMockingbird:Book {title:'To Kill a Mockingbird', published:1960, pages:281, img:'https://m.media-amazon.com/images/I/516IbJ592JL.jpg'})
CREATE (GoSetAWatchman:Book {title:'Go Set a Watchman', published:1960, pages:281, img:'https://images-na.ssl-images-amazon.com/images/I/71rl3UFZ0wL.jpg'})
CREATE (Gabriel:Author {name:"Gabriel García Márquez", born:1927})
CREATE (CienAniosDeSoledad:Book {title:'Cien Años de soledad', published:1967, pages:278, img:'https://images.penguinrandomhouse.com/cover/9780525562443'})
CREATE (ElAmorEnTiemposDelColera:Book {title:'El amor en tiempos del Colera', published:1985, pages:200, img:'https://images-na.ssl-images-amazon.com/images/I/915gud9ThAL.jpg'})
CREATE (Hermann:Author {name:"hermann hesse", born:1877})
CREATE (Steppenwolf:Book {title:'Steppenwolf', published:1927, pages:237, img:'https://images-na.ssl-images-amazon.com/images/I/A1uSx+gwXmL.jpg'})
CREATE (NarcissusAnGoldmund:Book {title:'Narcissus and Goldmund', published:1930, pages:320, img:'https://images-na.ssl-images-amazon.com/images/I/81wBIEW0APL.jpg'})
CREATE
  (Stephen)-[:WROTE]->(IT),
  (Stephen)-[:WROTE]->(TheShining),
  (Stephen)-[:WROTE]->(Cell),
  (Stephen)-[:WROTE]->(Misery),
  (Stephen)-[:WROTE]->(MrMercedes),
  (Stephen)-[:WROTE]->(LiseysStory),
  (John)-[:WROTE]->(TheAnalyst),
  (John)-[:WROTE]->(TheDeadStudent),
  (William)-[:WROTE]->(RomeoAndJuliet),
  (William)-[:WROTE]->(KingJohn),
  (William)-[:WROTE]->(JuliusCaesar),
  (Edgar)-[:WROTE]->(Berenice),
  (Harper)-[:WROTE]->(ToKillAMockingbird),
  (Harper)-[:WROTE]->(GoSetAWatchman),
  (Gabriel)-[:WROTE]->(CienAniosDeSoledad),
  (Gabriel)-[:WROTE]->(ElAmorEnTiemposDelColera),
  (Hermann)-[:WROTE]->(Steppenwolf),
  (Hermann)-[:WROTE]->(NarcissusAnGoldmund)
CREATE (English:Language {language:"English"})
CREATE (Spanish:Language {language:"Spanish"})
CREATE (German:Language {language:"German"})
CREATE (Italian:Language {language:"Italian"})
CREATE
  (IT)-[:WRITTEN_IN]->(English),
  (TheShining)-[:WRITTEN_IN]->(English),
  (Cell)-[:WRITTEN_IN]->(English),
  (Misery)-[:WRITTEN_IN]->(English),
  (MrMercedes)-[:WRITTEN_IN]->(English),
  (LiseysStory)-[:WRITTEN_IN]->(English),
  (TheAnalyst)-[:WRITTEN_IN]->(English),
  (TheDeadStudent)-[:WRITTEN_IN]->(English),
  (RomeoAndJuliet)-[:WRITTEN_IN]->(English),
  (KingJohn)-[:WRITTEN_IN]->(English),
  (JuliusCaesar)-[:WRITTEN_IN]->(English),
  (Berenice)-[:WRITTEN_IN]->(English),
  (ToKillAMockingbird)-[:WRITTEN_IN]->(English),
  (GoSetAWatchman)-[:WRITTEN_IN]->(English),
  (CienAniosDeSoledad)-[:WRITTEN_IN]->(Spanish),
  (ElAmorEnTiemposDelColera)-[:WRITTEN_IN]->(Spanish),
  (Steppenwolf)-[:WRITTEN_IN]->(German),
  (NarcissusAnGoldmund)-[:WRITTEN_IN]->(German)

CREATE (Horror:Category {category:"Horror"})
CREATE (ScienceFiction:Category {category:"Science Fiction"})
CREATE (PsychologicalThriller:Category {category:"Psychological Thriller"})
CREATE (Tragedy:Category {category:"Tragedy"})
CREATE (Novel:Category {category:"Novel"})
CREATE (Fiction:Category {category:"Fiction"})

CREATE
  (IT)-[:IS]->(Horror),
  (TheShining)-[:IS]->(Horror),
  (Cell)-[:IS]->(Horror),
  (Misery)-[:IS]->(Horror),
  (MrMercedes)-[:IS]->(Horror),
  (LiseysStory)-[:IS]->(Fiction),
  (TheAnalyst)-[:IS]->(PsychologicalThriller),
  (TheDeadStudent)-[:IS]->(PsychologicalThriller),
  (RomeoAndJuliet)-[:IS]->(Tragedy),
  (KingJohn)-[:IS]->(Tragedy),
  (JuliusCaesar)-[:IS]->(Tragedy),
  (Berenice)-[:IS]->(Horror),
  (ToKillAMockingbird)-[:IS]->(Fiction),
  (GoSetAWatchman)-[:IS]->(Fiction),
  (CienAniosDeSoledad)-[:IS]->(Novel),
  (ElAmorEnTiemposDelColera)-[:IS]->(Novel),
  (Steppenwolf)-[:IS]->(Fiction),
  (NarcissusAnGoldmund)-[:IS]->(Fiction)

