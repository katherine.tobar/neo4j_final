const express = require('express');
var neo4j = require('neo4j-driver')

const loginRoutes = express.Router({
    strict: true
});

loginRoutes.get('/', async(req, res)=>{
    res.render('login');
})
module.exports = loginRoutes;
