var express = require('express');
var neo4j = require('neo4j-driver')
const bodyParser = require('body-parser');
const cors = require('cors');

const bookRoutes = express.Router({
    strict: true
});

bookRoutes.use(cors());
bookRoutes.use(bodyParser.json());
bookRoutes.use(bodyParser.urlencoded({extended: false}));

var booksArr = [];
var bookData = [];
var authorData = [];
var booksByAuthor = [];
var cat = [];
var booksCat = [];
var nombre = '';
var categoria = '';

let username = " ";
let password = " ";

//obtencion de usuario y contraseña para poder inicializar el driver de neo4j
bookRoutes.post('/', async(req,res)=>{
    username = req.body.name;
    password = req.body.password;

        global.driver = neo4j.driver(
        'bolt://neo4j_library', 
        neo4j.auth.basic(`${username}`, `${password}`)
    )
    driver.close();
    res.redirect(`/book`)
})

//Se envia el usuario a la vista add para condicionar las vistas dependiendo de con que rol se accede
bookRoutes.get('/book/book/add', async(req,res)=>{
    res.render('add',{
        username:username
    });
})

//obtencion de todos los datos para agregar un nuevo libro con sus relaciones
bookRoutes.post('/book/book/add', async(req,res)=>{
    const session7 = driver.session();
    const session8 = driver.session();
    const session9 = driver.session();
    const session10 = driver.session();
    const session11 = driver.session();

    var titulo = req.body.title;
    var year = req.body.year;
    var pages = req.body.pages;
    var img = req.body.img;
    var nameAuthor = req.body.author;
    var birth = req.body.birth;
    var categoryBook = req.body.category;
    var language = req.body.language;

    session7
        .run('CREATE (b:Book {title:$titleBookParam, published:$yearParam, pages:$pagesParam, img:$imgParam}) RETURN b',{titleBookParam:titulo, yearParam:year, pagesParam:pages, imgParam:img})
        .catch((err)=>{
            console.log(err);
        });
    session8
        .run('MERGE (a:Author {name: $authorParam}) ON CREATE SET a.name = $authorParam,a.born= $birthParam ON MATCH SET a.name = $authorParam,a.born=$birthParam RETURN a',
        {authorParam:nameAuthor,birthParam:birth})
        .then((result)=>{
            session9
            .run('MATCH (a:Author {name:$authorParam}), (b:Book {title:$titleBookParam}) MERGE(a)-[r:WROTE]-(b) RETURN a,b',{authorParam:nameAuthor, titleBookParam:titulo})
            .then((result)=>{
                session10
                .run('MATCH (b:Book {title:$titleBookParam}), (c:Category {category:$categoryBookParam}) MERGE(b)-[r:IS]-(c) RETURN b,c', {titleBookParam:titulo, categoryBookParam:categoryBook})
                .catch((err)=>{
                    console.log(err);
                });        
            })
            .catch((err)=>{
                console.log(err);
            }); 
            console.log(req.body.author);
        })
        .catch((err)=>{
            console.log(err);
        }); 

    session11
        .run('MATCH (b:Book {title:$titleParam}), (l:Language {language:$languageParam}) MERGE(b)-[r:WRITTEN_IN]-(l) RETURN b,l',{languageParam:language,titleParam:titulo})
        .then((result)=>{
        })
        .catch((err)=>{
            console.log(err);
        });
        
        res.redirect(`/book`)

})

//Eliminación del libro seleccionado con todas sus relaciones
bookRoutes.get('/book/delete/:title', async(req,res)=>{
    const session = driver.session();

    var title=req.params.title;

    //Detach para eliminar relaciones y delete para eliminar el nodo
    session
    .run('MATCH (b:Book{title:$titleParam}) DETACH DELETE b', {titleParam:title})
    .then()
    .catch((err)=>{
        console.log(err);
    })
    res.redirect(`/book`)
})

//Vista principal. Mostrar todos los libros.
bookRoutes.get('/book', async (req,res)=>{
    const session = driver.session();

    session
    .run('MATCH(b:Book) RETURN b')
    .then((books)=>{
        books.records.forEach((record)=>{
            booksArr.push({
                id: record._fields[0].identity.low,
                title: record._fields[0].properties.title,
                published: record._fields[0].properties.published,
                img: record._fields[0].properties.img,
            })
            // console.log(record._fields[0].properties);
        })
    })
    .catch((err)=>{
        console.log(err);
    })
    .then(()=>{
            res.render('index',{
                books: booksArr,
                username:username
            })
            while(booksArr.length>0){
                booksArr.pop();
            }
    })
})

//direccionar a los datos del libro seleccionado
bookRoutes.get('/book/:title', async(req,res)=>{
    const session2 = driver.session();
    const session3 = driver.session();
    const session4 = driver.session();
    const session5 = driver.session();
    const session6 = driver.session();

    var title = req.params.title;
    session2
    // obtener datos del libro seleccionado
        .run('MATCH(b:Book{title:$titleParam}) RETURN b', {titleParam:title})
        .then((book)=>{

            book.records.forEach((record)=>{
                bookData.push({
                    id: record._fields[0].identity.low,
                    title: record._fields[0].properties.title,
                    published: record._fields[0].properties.published,
                    pages: record._fields[0].properties.pages,
                    img: record._fields[0].properties.img   
                })
                console.log(record._fields[0].properties);
            })
        })
        .catch((err)=>{
            console.log(err);
        })

    session3
    // Obtener nombre del autor relacionado con el libro
        .run('match (n:Book{title:$titleParam})MERGE(a)-[r:WROTE]-(n) Return a', {titleParam:title})
        .then((author)=>{

            author.records.forEach((record)=>{
                authorData.push({
                    id: record._fields[0].identity.low,
                    name: record._fields[0].properties.name,
                    born: record._fields[0].properties.born,
                })
                nombre=record._fields[0].properties.name
                console.log(record._fields[0].properties);
            })
        })
        .catch((err)=>{
            console.log(err);
        })

    session4
    // Obtener todos los libros que ha escrito ese autor
        .run('match (a:Author{name:$nameParam})MERGE(a)-[r:WROTE]-(n) Return n,a', {nameParam:nombre})
        .then((booksAuth)=>{

            booksAuth.records.forEach((record)=>{
                booksByAuthor.push({
                    id: record._fields[0].identity.low,
                    title: record._fields[0].properties.title,
                    published: record._fields[0].properties.published,
                    pages: record._fields[0].properties.pages,
                    img: record._fields[0].properties.img   
                })
                console.log(record._fields[0].properties);
            })
        })
        .catch((err)=>{
            console.log(err);
        }) 

    session5
    // obtener la categoria del libro
    .run('match (n:Book{title:$titleParam})MERGE(n)-[r:IS]-(a) Return a,n', {titleParam:title})
    .then((categories)=>{

        categories.records.forEach((record)=>{
            cat.push({
                id: record._fields[0].identity.low,
                category: record._fields[0].properties.category,
            })
            categoria=record._fields[0].properties.category
            console.log(record._fields[0].properties);
        })
        console.log(categoria)
    })
    .catch((err)=>{
        console.log(err);
    })

        session6
        // Obtener todos los libros pertenecientes a esa categoria
        .run('match (c:Category{category:$categoryParam})MERGE(a)-[r:IS]-(c) Return a,c', {categoryParam:categoria})
        .then((booksCate)=>{

            booksCate.records.forEach((record)=>{
                booksCat.push({
                    id: record._fields[0].identity.low,
                    title: record._fields[0].properties.title,
                    published: record._fields[0].properties.published,
                    pages: record._fields[0].properties.pages,
                    img: record._fields[0].properties.img   
                })
                console.log(booksCat)
            })
        })
        .catch((err)=>{
            console.log(err);
        })  

        .then(()=>{
            res.render('book',{
                booksAuth: booksByAuthor,
                booksCate: booksCat,
                categories: cat,
                authorData: authorData,
                book: bookData,
                title:title,
                nombre:nombre,
                categoria:categoria,
                username:username
            });

            // Limpiar todos los arrays para llenarlos con nueva información
            while( booksByAuthor.length>0 ||  booksCat.length>0 || cat.length>0 || authorData.length > 0 || bookData.length >0 ){
                booksByAuthor.pop(); 
                booksCat.pop();
                cat.pop();
                authorData.pop(); 
                bookData.pop(); 
            }
        })
})
module.exports = bookRoutes;

