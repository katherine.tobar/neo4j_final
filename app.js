const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const bookRoutes = require('./src/routes/bookRoutes.js')
const loginRoutes = require('./src/routes/loginRoutes')

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(cors());

app.use('/', bookRoutes);
app.use('/book/:title',bookRoutes);
app.use('/', loginRoutes);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname,'public')));

app.listen(5000);
console.log('server started on Port: 5000');

module.exports = app;